<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Auth::routes();

Route::get('/home', 'HomeController@index')->name('home');

Route::group(['middleware'=>'auth'],function(){
	Route::get('/','BarangController@index');
	Route::resource('barang','BarangController');
	Route::resource('customer','CustomerController');
	Route::resource('transaksi','TransaksiController');
	Route::resource('report','ReportController');	
});

// Auth::routes();

//Route::get('/home', 'HomeController@index')->name('home');
