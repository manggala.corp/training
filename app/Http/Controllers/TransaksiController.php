<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Transaksi;
use App\Customer;
use App\Barang;
use App\TrTransaksi;



class TransaksiController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
        $data['transaksi'] = Transaksi::paginate(10);
        return view('transaksi.index',$data);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
        $data['customer'] = Customer::all();
        $data['barang'] = Barang::all();
        return view('transaksi.create',$data);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //

        if(count($request->id_barang) > 0)
        {
            $transaksi = new Transaksi;
            $transaksi->id_customer = $request->id_customer;
            $transaksi->tgl_transaksi = $request->tgl_transaksi;
            $transaksi->no_nota = $request->no_nota;
            $transaksi->keterangan = $request->keterangan;
            $transaksi->save();

            for($i=0;count($request->id_barang) > $i ;$i++):
                $tr_transaksi = new TrTransaksi;
                $tr_transaksi->id_barang = $request->id_barang[$i];
                $tr_transaksi->id_transaksi = $transaksi->id_transaksi;
                $tr_transaksi->save();
            endfor;



            return redirect('transaksi')->with('status','Data transaksi berhasil ditambahkan');;
        }
        else
        {
            return redirect('transaksi')->with('status','Data transaksi gagal ditambahkan');;   
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $transaksi = Transaksi::with('customer')
                    ->where('id_transaksi',$id)
                    ->first();
        $data['transaksi'] = $transaksi;
        $data['barang'] = TrTransaksi::with('barang')->where('id_transaksi',$id)->get();
        return view('transaksi.show',$data);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
        $data = Transaksi::find($id);
        $data->delete();
        return redirect('transaksi');

    }
}
