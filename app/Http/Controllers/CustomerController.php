<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Customer;


class CustomerController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
        $data['customer'] = Customer::paginate(10);
        return view('customer.index',$data);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
        return view('customer.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
        $validatedData = $request->validate([
            'nama_customer' => 'required|max:255',
            'alamat_customer' => 'required',

        ]);

        $customer = new Customer;
        $customer->nama_customer = $request->nama_customer;
        $customer->alamat_customer = $request->alamat_customer;
        $customer->keterangan = $request->keterangan;
        $customer->save();

        return redirect('customer')->with('status','Data customer berhasil ditambahkan');

    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
       
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
        $data['customer'] = Customer::find($id);
        return view('customer.edit',$data);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
        $validatedData = $request->validate([
            'nama_customer' => 'required|max:255',
            'alamat_customer' => 'required',
        ]);

        $customer = Customer::find($id);
        $customer->nama_customer = $request->nama_customer;
        $customer->alamat_customer = $request->alamat_customer;
        $customer->keterangan = $request->keterangan;
        $customer->save();
        
        return redirect('customer')->with('status','Data customer berhasil diupdate');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
        $customer = Customer::find($id);
        $customer->delete();

        return redirect('customer')->with('status','Data customer berhasil dihapus');
    }
}
