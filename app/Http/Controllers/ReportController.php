<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Transaksi;
use App\Customer;
use App\Barang;
use App\TrTransaksi;
use Barryvdh\DomPDF\Facade as PDF;


class ReportController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
        return view('report.index');

    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create(Request $request)
    {
        //
        $dari = $request->tgl_dari;
        $sampai = $request->tgl_sampai;
        $data['tgl_dari'] = $request->tgl_dari;
        $data['tgl_sampai'] = $request->tgl_sampai;
        $data['barang'] = TrTransaksi::with('transaksi','barang')
        ->whereHas('transaksi',function($query) use ($dari,$sampai){
            $query->whereBetween('tgl_transaksi',[$dari,$sampai]);
        })
        ->get();
        return view('report.create',$data);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show(Request $request,$id)
    {
        //
        $dari = $request->tgl_dari;
        $sampai = $request->tgl_sampai;
        $data = TrTransaksi::with('transaksi','barang')
        ->whereHas('transaksi',function($query) use ($dari,$sampai){
            $query->whereBetween('tgl_transaksi',[$dari,$sampai]);
        })
        ->get();

        $pdf = PDF::loadview('report/cetak',['dataExport'=>$data]);
        return $pdf->download('report'.date('Ymd').'.pdf');
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
