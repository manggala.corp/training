<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class TrTransaksi extends Model
{
    //
   protected $primaryKey = "id_tr_transaksi";
   protected $table ="tr_transaksi";

    public function transaksi()
    {
        return $this->hasOne(Transaksi::class, 'id_transaksi','id_transaksi');
    }
    public function barang()
    {
        return $this->hasOne(Barang::class, 'id_barang','id_barang');
    }
    

}



