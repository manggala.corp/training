<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Customer extends Model
{
    //
    protected $primaryKey = "id_customer";
    protected $table ="customers";

    public function transaksi()
    {
    	return $this->belongsTo(Transaksi::class,'id_transaksi');
    }


    public function tr_transaksi()
    {
    	return $this->hasMany(TrTransaksi::class,'id_transaksi');
    }

}
