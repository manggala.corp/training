<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Transaksi extends Model
{
    //
    protected $primaryKey = "id_transaksi";
    protected $table ="transaksi";

    public function customer()
    {
    	return $this->hasOne(Customer::class,'id_customer','id_customer');
    }

    public function tr_transaksi()
    {
    	return $this->hasMany(TrTransaksi::class,'id_transaksi','id_barang');
    }

    public function barang()
    {
    	return $this->hasMany(Barang::class,'id_barang','id_barang');
    }

    
}
