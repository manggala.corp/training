<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Barang extends Model
{
    //
    protected $primaryKey = "id_barang";
    protected $table ="barang";

    public function tr_transaksi()
    {
    	return $this->belongsTo(TrTransaksi::class,'id_tr_transaksi');
    }

}
