<!-- LAYOUT -->
@extends('layout')

<!-- TITLE -->
@section('title')
	Create Barang
@endsection


<!-- CONTENT -->
@section('content')
<div>
    <div class="x_panel">
      <div class="x_title">
        <h2> Master Barang </h2>
        <ul class="nav navbar-right panel_toolbox">
          <li><a class="collapse-link"><i class="fa fa-chevron-up"></i></a>
          </li>
          <li class="dropdown">
            <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-expanded="false"><i class="fa fa-wrench"></i></a>
            <ul class="dropdown-menu" role="menu">
              <li><a href="#">Settings 1</a>
              </li>
              <li><a href="#">Settings 2</a>
              </li>
            </ul>
          </li>
          <li><a class="close-link"><i class="fa fa-close"></i></a>
          </li>
        </ul>
        <div class="clearfix"></div>
      </div>

      <div class="x_content">
      @if ($errors->any())
        <div class="alert alert-danger">
            <ul>
                @foreach ($errors->all() as $error)
                    <li>{{ $error }}</li>
                @endforeach
            </ul>
        </div>
      @endif
     
      <form id="form_barang" action="{{route('barang.update',$barang->id_barang)}}"  method="POST" data-parsley-validate class="form-horizontal form-label-left">
            {{ csrf_field() }}
            {{ method_field('PUT') }}
         <div class="form-group">
            <label class="col-form-label col-sm-2" for="nama_barang"> Nama Barang   <span class="required" style="color:red">*</span>
            </label>
            <div class="col-md-6">
            <input type="text" id="nama_barang" name="nama_barang" value="{{$barang->nama_barang}}" required="required" class="form-control">
            </div>
          </div>

          <div class="form-group">
              <label class="col-form-label col-sm-2" for="harga_beli"> Harga beli  <span class="required" style="color:red">*</span>
              </label>
              <div class="col-md-6">
                <input type="text" id="harga_beli" name="harga_beli" value="{{$barang->harga_beli}}" required="required" class="form-control">
              </div>
            </div>


          <div class="form-group">
            <label class="col-form-label col-sm-2" for="harga_jual"> Harga Jual  <span class="required" style="color:red">*</span>
            </label>
            <div class="col-md-6">
              <input type="text" id="harga_jual" name="harga_jual" value="{{$barang->harga_jual}}" required="required" class="form-control">
            </div>
          </div>

           <div class="form-group">
            <label class="col-form-label col-sm-2" for="keterangan"> Keterangan
            </label>
            <div class="col-md-6">
              <textarea type="text" id="keterangan"  name="keterangan"  class="form-control">{{$barang->keterangan}}</textarea>
            </div>
          </div>

          <div class="ln_solid"></div>
          <div class="form-group">
            <div class="col-md-6 col-sm-6 col-xs-12 col-md-offset-3">
              <a class="btn btn-default" type="button" href="{{ route('barang.index') }}" >Batal</a>
              <button type="submit"  id=""  class="btn btn-success">Simpan</button>
            </div>
          </div>
        </form>
      </div>
    </div>
</div>
@endsection

