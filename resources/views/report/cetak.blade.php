<!DOCTYPE html>
<html>
<head>
	<title>Membuat Laporan PDF Dengan DOMPDF Laravel</title>
	<meta http-equiv="Content-Type" content="text/html; charset=utf-8"/>
	<link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css" integrity="sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T" crossorigin="anonymous">
</head>
<body>
<table id="tableTransaksi" class="table table-striped jambo_table bulk_action" cellspacing="0" width="100%">
<thead>
  <tr class="headings">
    <th class="column-title" width="5%">No </th>
    <th class="column-title"> Nama Barang </th>
    <th class="column-title">  Harga </th>
  </tr>
</thead>   
<tbody>
  @php
    $no=1;   
    $tot=0;   
  @endphp
	@foreach($dataExport as $br)
	<tr>
		<td>{{$no}}</td>
		<td>{{$br->barang->nama_barang}}</td>
		<td>{{$br->barang->harga_jual}}</td>
	</tr>
  
	@php
      $tot += $br->barang->harga_jual;
      $no++;
    @endphp
	@endforeach
  <tr>
      <th colspan="2"> Total</th>
      <th > {{$tot}}</th>
  </tr>
</tbody> 
</table>
</body>
</html>

