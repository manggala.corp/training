
<!-- LAYOUT -->
@extends('layout')

<!-- TITLE -->
@section('title')
  Transaksi
@endsection
@php
    $tgl_dari = isset($_GET['tgl_dari']) ?  $_GET['tgl_dari'] : "" ;
    $tgl_sampai = isset($_GET['tgl_sampai']) ?  $_GET['tgl_sampai']:  "" ;
@endphp

<!-- CONTENT -->
@section('content')
<div>
    <div class="x_panel">
      <div class="x_title">
        <h2> Transaksi </h2>
        <ul class="nav navbar-right panel_toolbox">
          <li><a class="collapse-link"><i class="fa fa-chevron-up"></i></a>
          </li>
          <li class="dropdown">
            <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-expanded="false"><i class="fa fa-wrench"></i></a>
            <ul class="dropdown-menu" role="menu">
              <li><a href="#">Settings 1</a>
              </li>
              <li><a href="#">Settings 2</a>
              </li>
            </ul>
          </li>
          <li><a class="close-link"><i class="fa fa-close"></i></a>
          </li>
        </ul>
        <div class="clearfix"></div>
      </div>

      <div class="x_content">
      @if ($errors->any())
        <div class="alert alert-danger">
            <ul>
                @foreach ($errors->all() as $error)
                    <li>{{ $error }}</li>
                @endforeach
            </ul>
        </div>
      @endif  
     
      <form id="form_barang" action="{{route('report.create')}}"  method="GET" data-parsley-validate class="form-horizontal form-label-left">
        @csrf

         <div class="form-group">
            <label class="col-form-label col-sm-2" for="tgl_dari"> Tanggal Dari   
            </label>
            <div class="col-md-6">
            	 <input type="text" id="tgl_dari" name="tgl_dari" value="{{$tgl_dari}}" required="required" class="form-control">

            </div>
          </div> 


          <div class="form-group">
            <label class="col-form-label col-sm-2" for="tgl_sampai"> Tanggal Sampai   
            </label>
            <div class="col-md-6">
            	 <input type="text" id="tgl_sampai" name="tgl_sampai" value="{{$tgl_sampai}}" required="required" class="form-control">

            </div>
          </div>

          <div class="form-group">
            <label class="col-form-label col-sm-2" for="tgl_sampai">    
            </label>
            <div class="col-md-6">
            	<button  type="submit" class="btn btn-success" data-toggle="modal" data-target="#modalBarang"> Cari  </button>
            </div>
          </div>
          
        <div class="form-group">
          <table id="tableTransaksi" class="table table-striped jambo_table bulk_action" cellspacing="0" width="100%">
            <thead>
              <tr class="headings">
                <th class="column-title" width="5%">No </th>
                <th class="column-title"> Nama Barang </th>
                <th class="column-title">  Harga </th>
              </tr>
            </thead>   
            <tbody>
              @php
                $no=1;   
                $tot=0;   
              @endphp
            	@foreach($barang as $br)
            	<tr>
            		<td>{{$no}}</td>
            		<td>{{$br->barang->nama_barang}}</td>
            		<td>{{$br->barang->harga_jual}}</td>
            	</tr>
              
            	@php
                  $tot += $br->barang->harga_jual;
                  $no++;
                @endphp
            	@endforeach
              <tr>
                  <th colspan="2"> Total</th>
                  <th > {{$tot}}</th>
              </tr>
            </tbody> 
          </table>
        </div>

          <div class="ln_solid"></div>
          <div class="form-group">
            <div class="col-md-6 col-sm-6 col-xs-12 col-md-offset-3">
              <a class="btn btn-default" type="button" href="{{ route('report.index') }}" >Batal</a>
              <a href="{{route('report.show',['id'=>1,'tgl_dari'=>$tgl_dari,'tgl_sampai'=>$tgl_sampai])}}" class="btn btn-success"> Cetak </a>
            </div>
          </div>
        </form>

       
      </div>
    </div>
</div>
@endsection



@section('script')
  <script type="text/javascript">
    $('#tgl_dari').datetimepicker({
        format: 'DD-MM-YYYY'
    });

    $('#tgl_sampai').datetimepicker({
        format: 'DD-MM-YYYY'
    });

  </script>
@endsection