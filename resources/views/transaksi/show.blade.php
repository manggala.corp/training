
<!-- LAYOUT -->
@extends('layout')

<!-- TITLE -->
@section('title')
  Transaksi
@endsection


<!-- CONTENT -->
@section('content')
<div>
    <div class="x_panel">
      <div class="x_title">
        <h2> Transaksi </h2>
        <ul class="nav navbar-right panel_toolbox">
          <li><a class="collapse-link"><i class="fa fa-chevron-up"></i></a>
          </li>
          <li class="dropdown">
            <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-expanded="false"><i class="fa fa-wrench"></i></a>
            <ul class="dropdown-menu" role="menu">
              <li><a href="#">Settings 1</a>
              </li>
              <li><a href="#">Settings 2</a>
              </li>
            </ul>
          </li>
          <li><a class="close-link"><i class="fa fa-close"></i></a>
          </li>
        </ul>
        <div class="clearfix"></div>
      </div>

      <div class="x_content">
      @if ($errors->any())
        <div class="alert alert-danger">
            <ul>
                @foreach ($errors->all() as $error)
                    <li>{{ $error }}</li>
                @endforeach
            </ul>
        </div>
      @endif  
     
      <form id="form_barang" action="{{route('transaksi.store')}}"  method="POST" data-parsley-validate class="form-horizontal form-label-left">
        @csrf

         <div class="form-group">
            <label class="col-form-label col-sm-2" for="tgl_transaksi"> Tanggal Transaksi   
            </label>
            <div class="col-md-6">
                {{$transaksi->tgl_transaksi}}
            </div>
          </div>

          <div class="form-group">
              <label class="col-form-label col-sm-2" for="no_nota">  Customer 
              </label>
              <div class="col-md-6">
                   {{$transaksi->customer->nama_customer}}
              </div>
          </div>

          <div class="form-group">
              <label class="col-form-label col-sm-2" for="no_nota">  No Nota 
              </label>
              <div class="col-md-6">
                {{$transaksi->no_nota}}
              </div>
          </div>


          <div class="form-group">
            <label class="col-form-label col-sm-2" for="keterangan"> Keterangan
            </label>
            <div class="col-md-6">
                  {{$transaksi->keterangan}}
            </div>
          </div>  
          <br><br>
      
        <div class="form-group">
          <table id="tableTransaksi" class="table table-striped jambo_table bulk_action" cellspacing="0" width="100%">
            <thead>
              <tr class="headings">
                <th class="column-title" width="5%">No </th>
                <th class="column-title"> Nama Barang </th>
                <th class="column-title">  Harga </th>
                </th>
              </tr>
            </thead>   
            <tbody>
              @php
                $no = 1;
              @endphp
                @foreach($barang as $br)
                  <tr>
                    <td>{{$no}}</td>
                    <td> {{$br->barang->nama_barang}} </td>
                    <td> {{$br->barang->harga_jual}} </td>
                  </tr>
                  @php
                   $no++ 
                  @endphp
                @endforeach
            </tbody> 
          </table>
        </div>

          <div class="ln_solid"></div>
          <div class="form-group">
            <div class="col-md-6 col-sm-6 col-xs-12 col-md-offset-3">
              <a class="btn btn-default" type="button" href="{{ route('transaksi.index') }}" >Batal</a>
              <button type="submit"  id=""  class="btn btn-success">Simpan</button>
            </div>
          </div>
        </form>

       
      </div>
    </div>
</div>


<!-- Modal -->
<div class="modal fade" id="modalBarang" tabindex="-1" role="dialog" aria-labelledby="modalBarangTitle" aria-hidden="true">
  <div class="modal-dialog modal-dialog-centered" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title" id="exampleModalLongTitle">Modal title</h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body">
          <table id="tableBarang" class="table table-striped jambo_table bulk_action" cellspacing="0" width="100%">
            <thead>
              <tr class="headings">
                <th class="column-title" width="5%">No </th>
                <th class="column-title">Nama Barang </th>
                <th class="column-title"> Harga Jual </th>
                <th class="column-title"> Keterangan </th>
                <th class="column-title no-link last" width="150px;"><span class="nobr">Action</span>
                </th>
              </tr>
            </thead>
            <tbody>
              {{-- @php
                $no=1;   
              @endphp
              @foreach ($barang as $br)
                <tr>
                  <td>{{$no}}</td>
                  <td>{{$br->nama_barang}}</td>
                  <td>{{$br->harga_jual}}</td>
                  <td>{{$br->keterangan}}</td>
                  <td> 
                    <button onclick="pilihBarang(this,'{{$br->id_barang}}')" class="btn btn-primary pilihBarang"> Pilih </button>
                  </td>
                </tr>    
                @php
                  $no++    
                @endphp
              @endforeach
               --}}

            </tbody>
          </table>
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
      </div>
    </div>
  </div>
</div>
@endsection

@section('script')
  <script type="text/javascript">
    $('#tgl_transaksi').datetimepicker({
        format: 'DD-MM-YYYY'
    });

    // $('#tableBarang').on('click','.pil')

    function pilihBarang(e,id)
    {
       var tableTransaksi = $('#tableTransaksi tbody');
       var current = $(e).closest('tr');

        var col1 = current.find('td:eq(0)').text();
        var col2 = current.find('td:eq(1)').text();
        var col3 = current.find('td:eq(2)').text();
        var col4 = current.find('td:eq(3)').text();


        tableTransaksi.append('<tr>'+
            '<td>'+col1+'</td>'+
            '<td>'+col2+'</td>'+
            '<td>'+col3+'</td>'+
            '<input type="hidden" name="id_barang[]" value="'+id+'">'+
          '</tr>');

        $('#modalBarang').modal('hide');


    }
  </script>
@endsection




