
<!-- LAYOUT -->
@extends('layout')

<!-- TITLE -->
@section('title')
	 Transaksi
@endsection


<!-- CONTENT -->
@section('content')
<div>
    <div class="x_panel">
      <div class="x_title">
        <h2>  Transaksi </h2>
        <ul class="nav navbar-right panel_toolbox">
          <li><a class="collapse-link"><i class="fa fa-chevron-up"></i></a>
          </li>
          <li class="dropdown">
            <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-expanded="false"><i class="fa fa-wrench"></i></a>
            <ul class="dropdown-menu" role="menu">
              <li><a href="#">Settings 1</a>
              </li>
              <li><a href="#">Settings 2</a>
              </li>
            </ul>
          </li>
          <li><a class="close-link"><i class="fa fa-close"></i></a>
          </li>
        </ul>
        <div class="clearfix"></div>
      </div>

      @if (session('status'))
        <div class="alert alert-success">
            {{ session('status') }}
        </div>
      @endif

      <div  class="col-sm-12">
          <a class="btn btn-success" id=""  href="{{route('transaksi.create')}}"><i class="fa fa-plus"></i> Tambah  </a>
      </div>
      <div class="x_content">
            <table id="" class="table table-striped jambo_table bulk_action" cellspacing="0" width="100%">
            <thead>
              <tr class="headings">
                <th class="column-title" width="5%">No </th>
                <th class="column-title">Tanggal Transaksi </th>
                <th class="column-title">  No Nota </th>
                <th class="column-title"> Keterangan </th>
                <th class="column-title no-link last" width="150px;"><span class="nobr">Action</span>
                </th>
              </tr>
            </thead>
            <tbody>
              @php
                $no=1;   
              @endphp
              @foreach ($transaksi as $tr)
                <tr>
                  <td>{{$no}}</td>
                  <td>{{$tr->tgl_transaksi}}</td>
                  <td>{{$tr->no_nota}}</td>
                  <td>{{$tr->keterangan}}</td>
                <td> <a class="btn btn-primary" href="{{route('transaksi.show',$tr->id_transaksi)}}"> View </a>
                   {{-- <a class="btn btn-danger" href="{{url('Transaksi/'.$tr->id_transaksi)}}"> Delete </a>  --}}
                   <form action="{{ route('transaksi.destroy', $tr->id_transaksi) }}" method="POST" style="display:inline">
                      {{ csrf_field() }}
                      {{ method_field('DELETE') }}
                      <button class="btn btn-danger btn-xs">
                        <span>DELETE</span>
                      </button>
                    </form>
                  
                  </td>
                </tr>    
                @php
                  $no++    
                @endphp
              @endforeach
            </tbody>
          </table>
      </div>
    </div>
</div>

@endsection

