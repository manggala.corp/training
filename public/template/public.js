
/*
  Created By : Arief Manggala Putra
  Date  : 27 maret 2017
  My Website : manggalacorp.com
  My Email  : manggala.corp@gmail.com
  description : this function for a dynamic in my application

  */
var table;
var method;

  //function for reload databtable
  $(function(){

    table =  $('#table_'+table_js).DataTable({
      "processing":true,
      "serverSide":true,
      "order":[[0,'desc']],
      "ajax":{
        "url":url_js+table_js+'/ajax_datatable',
        'type':'POST'
      }
    })

    //for date picker and timepicker
      $('.time_picker').timepicker({ 'timeFormat': 'H:i:s' });

        $('.date_picker').datepicker({
          autoclose: true,
          changeMonth: true,
          changeYear: true,
          showButtonPanel: true,
          todayHighlight: true,
          dateFormat:'dd-mm-yy',
        });
          $('#harga_tiket').priceFormat({
            prefix:'',
            thousandsSeparator:'.',
            centsLimit: 0,
        });

          //for key number
      $(".number_valid").on("keypress", function (event) {
          var regex = /[0-9+]/g;
          var key = String.fromCharCode(event.which);
          if (regex.test(key) || event.keyCode == 8 || event.keyCode == 9 || event.keyCode == 37 || event.keyCode == 39) {
              return true;
          }
            return false;
      });

      $('.form_select2').select2();


  });

  // for ajax tingkat kelas
  $('[name="id_unit"]').change(function(){
     var  id_unit = $('[name="id_unit"]').val();
     var tingkat_kelas = $('[name="id_tingkat_kelas"]');

      $.post(url_js+'app/ajax_get_tingkat_kelas',{id_unit:id_unit},function(resp){
          if(resp.status=='true')
          {
            tingkat_kelas.empty();
            tingkat_kelas.append('<option value=""> - PIlih Tingkat Kelas - </option>');
            $.each(resp.data,function(index,val){
              tingkat_kelas.append('<option value="'+val.id_tingkat_kelas+'"> '+val.nama_tingkat_kelas+' </option>');
            });
          }
          else
          {
              tingkat_kelas.empty();
              tingkat_kelas.append('<option value=""> - Tidak ada data - </option>');
          }

      });

  });


  // for ajax bangunan
  $('[name="id_bangunan"]').change(function(){
     var  id_bangunan = $('[name="id_bangunan"]').val();
     var lantai_bangunan = $('[name="id_lantai_bangunan"]');

      $.post(url_js+'app/ajax_get_lantai_bangunan',{id_bangunan:id_bangunan},function(resp){
          if(resp.status=='true')
          {
            lantai_bangunan.empty();
            lantai_bangunan.append('<option value=""> - PIlih Lantai - </option>');
            $.each(resp.data,function(index,val){
              lantai_bangunan.append('<option value="'+val.id_lantai_bangunan+'"> '+val.nama_lantai_bangunan+' </option>');
            });
          }
          else
          {
              lantai_bangunan.empty();
              lantai_bangunan.append('<option value=""> - Tidak ada data - </option>');
          }

      });

  });

// function get for unit ini jika ada jenjang pendidikan
  function get_unit()
  {
     var  id_jenjang_pendidikan = $('[name="id_jenjang_pendidikan"]').val();
     var id_unit = $('[name="id_unit"]');

      $.post(url_js+'app/ajax_get_unit',{id_jenjang_pendidikan:id_jenjang_pendidikan,unit:unit},function(resp){
          if(resp.status=='true')
          {
            id_unit.empty();

            id_unit.append('<option value=""> - PIlih Unit - </option>');
            $.each(resp.data,function(index,val){
              id_unit.append('<option value="'+val.id_unit+'"> '+val.nama_unit+' </option>');
            });
          }
          else
          {
              id_unit.empty();
              id_unit.append('<option value=""> - Tidak ada data - </option>');
          }

      });
  }




  //function public delete
  function delete_data(id){
      swal({
        title: "Delete Data",
        text: "Apakah Kamu Yakin Hapus Data Ini!",
        type: "warning",
        showCancelButton: true,
        confirmButtonColor: "#DD6B55",
        confirmButtonText: "Ya, Hapus Data!",
        closeOnConfirm: false
      },
      function(){
        var id_delete = id;
        $.post(url_js+table_js+'/action/delete',{id_delete:id_delete},function(turn_data){
          if(turn_data.data==1){
            swal("Hapus data berhasil!","", "success");
             reload_table();
             location.reload();
          }else{
            swal({
                  title: "Error",
                  type: "error",
                  text: 'Function Error',
                  html: true
                });
          }


        }).fail( function(xhr, textStatus, errorThrown) {
            swal({
                  title: "Error",
                  type: "error",
                  text: 'Function Error',
                  html: true
                });
            //location.reload();

        });
      });
  }


// untuk edit data di modal
  function edit_data(id)
  {
      data = new FormData($("#form_"+table_js)[0]);
      url = url_js+table_js+'/action/edit/'+id;

       //ajax untuk save users
      $.ajax({
          url: url,
          data: data,
          type:"POST",
          beforeSend:function(){

            swal({
              title: "Loading!",
              text: "Mohon Tunggu",
            });
          },
          success:function(turn_data){
            // console.log(turn_data);

            if(turn_data.data=='1')
            {

             swal("Input data berhasil!","", "success");
             location.reload();
            }
            else
            {
                swal({
                  title: "Error",
                  type: "error",
                  text: turn_data,
                  html: true
                });
            }
          },
          error: function (xhr, ajaxOptions, thrownError) {
              swal({
                  title: "Error",
                  type: "error",
                  text: 'Function Error',
                  html: true
                });
              //location.reload();
          },
          cache:false,
          contentType:false,
          processData:false,
      });

  }


    //function for save users
  function tambah_data(modal_js=""){

    

      data = new FormData($("#form_"+table_js)[0]);

      // kondisi untuk cek dia save apa edit
      if(param =="save")
      {
        url = url_js+table_js+'/action/tambah';
      }

      else
      {
        url = url_js+table_js+'/action/edit/'+param;
      }


      //ajax untuk save users
      $.ajax({
          url: url,
          data: data,
          type:"POST",
          beforeSend:function(){

            swal({
              title: "Loading!",
              text: "Mohon Tunggu",
            });
          },
          success:function(turn_data){
            
            if(modal_js!="" && turn_data.data=="1")
            {
              location.reload();
            }

            else if(turn_data.data==1)
            {
              swal("Input data berhasil!","", "success");
              location.reload();
              $(location).attr('href',url_js+table_js);
            }
            else
            {
                swal({
                  title: "Error",
                  type: "error",
                  text: turn_data,
                  html: true
                });
            }
          },
             error: function (xhr, ajaxOptions, thrownError) {
              swal({
                  title: "Error",
                  type: "error",
                  text: 'Function Error',
                  html: true
                });
             //location.reload();
          },
          cache:false,
          contentType:false,
          processData:false,
      });

  }

  // fungsi untuk popup
  function pop_up(url)
  {
    window.open(url, 'title', "toolbar=no,scrollbars=no,resizable=no,top=200,left=300,width=700,height=500",'_blank');

  }
