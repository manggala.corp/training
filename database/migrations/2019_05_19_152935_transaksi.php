<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class Transaksi extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('transaksi', function (Blueprint $table) {
            //
            $table->increments('id_transaksi');
            $table->integer('id_customer')->unsigned()->index();
            $table->foreign('id_customer')->references('id_customer')->on('customers')->onDelete('cascade');
            $table->string('tgl_transaksi')->nullable();
            $table->string('no_nota')->nullable();
            $table->text('keterangan')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('transaksi');
    }
}
